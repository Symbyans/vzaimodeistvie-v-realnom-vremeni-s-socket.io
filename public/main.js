var port = 3000;
var socket = io.connect('http://localhost:' + port);


socket.on('userName', function(userName){
	console.log('You\'r username is => ' + userName);
	document.getElementById('textarea').value += `You\'r login is ${userName}\n`;
});


socket.on('newUser', function(userName){
	console.log('New user has been connected to chat | ' + userName);
	document.getElementById('textarea').value += `${userName} connected\n`;
});



function click_on_button() {
	let msg = document.getElementById('input').value;
 	if(msg !== ''){
 		socket.emit('message', msg);
 		document.getElementById('input').value = null;
 	}else{
 		document.getElementById('textarea').value += '(!) Enter the message text \n';
 	}
}


socket.on('messageToClients', function(msg, name){
	console.log(name + ' | => ' + msg);
	document.getElementById('textarea').value += `${name}: ${msg} \n`;
});